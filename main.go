package main

import "time"

type UserApiKey struct {
	ID           uint `json:"id"`
	Name         string `json:"name"`
	Source uint `json:"source"` // = 2
	LastSyncDate *time.Time `json:"last_sync_date"`
	CreationDate *time.Time `json:"creation_date"`
	NextSyncDate *time.Time `json:"next_sync_date"`
	UserId       uint      `json:"-"`
	Key          string    `json:"-"  gorm:"column:api_key"`
	Secret       string    `json:"-" gorm:"column:api_secret"`
	//Settings *UserApiKeySettings `json:"settings" gorm:"column:settings"`
}

const (
	SourceKraken = 1
	SourceBinance = 2
)

const (
	LedgerTypeBuy = 1
	LedgerTypeSell = 2
	LedgerTypeDeposit = 3
	LedgerTypeWithdraw = 4
)

//target internal format
type UserLedger struct {
	Id uint64 `json:"id"`
	AssetPair string `json:"asset_pair"` // format: BTC-EUR, XXX-XXX , ADA-EUR, ETH-BTC , etc
	UserId uint `json:"user_id"` // comes from key
	Source int8 `json:"source"` // = 2
	Type        uint8 `json:"type"` // LedgerTypeBuy etc. add if missing
	AssetIn string `json:"asset_in"` //asset consumed by transaction, ie what is sold
	AssetOut string `json:"asset_out"` //asset produced by transaction, ie what is bought or minted
	AssetInDiff float64 `json:"asset_in_diff"` // example: buy BTC-EUR 0.1 btc for 100 eur, diff = -100.0 EUR
	AssetOutDiff float64 `json:"asset_out_diff"` // example: buy BTC-EUR 0.1 btc, diff = 0.1
	FeeAsset string // EUR,BTC,etc
	Fees              float64    `json:"fees"` //
	AssetPrice        float64    `json:"asset_price"` // momentum spot price when ledger happened (ok to supply only for trades)
	Date              time.Time  `json:"date"` //date of ledger
	LedgerId          string     `json:"ledger_id"` // binance ref
	ReferenceId       string     `json:"reference_id"` // binance ref
}

func BinanceSync(apiKey UserApiKey) {
	SyncBinanceLedger(apiKey)

	TransformBinanceLedgerIntoUserLedger(apiKey)
}

func SyncBinanceLedger(apiKey UserApiKey) {
	//
}

func TransformBinanceLedgerIntoUserLedger(apiKey UserApiKey) {
	//
}

func main() {
	apiKey := UserApiKey{ Key: "", Secret: "", UserId: 1	}
	BinanceSync( apiKey )

}