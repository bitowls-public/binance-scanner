
# Task:

* integrate library to fetch binance account ledger (trade,deposit,withdraw,stake,etc)
* implement logic  to sync ledger from binance to database in raw format(close to binance struct)  via gorm with two modes:

1) from scratch
2) from current position of db

* implement logic to gradually tansform raw binance saved format into internal db format (UserLedger struct)
* write test to ensure that your code is working and behavior is fixated.

INPUT: mocked binance response with all possible kinds of ledger operations

OUTPUT: UserLedger entries saved in db

ensure that mocked response numbers are matching in UserLedger db

# Constraints:

* use GORM for db operations
* To create database tables use https://github.com/golang-migrate/migrate or similar in a cli mode or as argument to current process
* respect binance response codes to backoff with retry
* stop sync process on critical errors/many retries without success   
* code should be able to be stopped & resumed from ANY point without data corruptions
* follow the format in existing data structs